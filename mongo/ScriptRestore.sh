#!/bin/sh

#  Script.sh
#  
#
#  Created by Kapil Kabra on 4/23/21.
#  sample command => sh ScriptRestoreTest.sh -e localhost -u migrationdb -p migrationdb -d migrationdb -o 'D:\\MigrationBackupTEST\\wruat3\\'
usage()
{
	echo -e "\n---------- Help ----------------- \n"	
	echo -e "Usage: $0 -e db_host -u dbuser -p dbpassword -d dbschema  -o dump folder path \n\n"
        echo -e "./ScriptRestore.sh -e localhost -u wruat -p wruat -d wruat -o './output/wruat3/'"
}
SCRIPT_ARGS=$#
if [ "${SCRIPT_ARGS}" -lt "5" ]
then
	echo -e "\n only ${SCRIPT_ARGS} arguments?\n"
	usage
	exit 1
fi

while getopts p:e:u:d:o:i: option
do
        case "${option}"
        in
       	    p) DBPWD=${OPTARG};;
	    	e) DBHOST=${OPTARG};;
            u) DBUSER=${OPTARG};;
            d) DBSCHEMA=${OPTARG};;
            o) OUTPUT_FOLDER=${OPTARG};;
            i) URI=${OPTARG};;
        esac
done
echo $DBHOST $DBUSER $DBPWD $DBSCHEMA $COMPANYNAME
#--username $DBUSER --password $DBPWD 
mongoCollection=(assignmentHistory contract rider route routeDefinition serviceZone tripSummary vizzon vizzonHash entityId levelOfService  preschedule profile settingDefinition systemSettings)


for collection in "${mongoCollection[@]}"
do
	if [ -f ${OUTPUT_FOLDER}$collection.bson ]; then
    	echo "$collection"
    	mongorestore --uri $URI --collection $collection ${OUTPUT_FOLDER}$collection.bson
    fi
done
