#!/bin/sh

#  Script.sh
#  
#  sh ScriptBackupTest.sh -e 10.100.30.132 -u wruat3 -p wruat3 -d wruat3 -c STC -r 2020-07-01T00:00:00.001Z -a 2020-07-01T00:00:00.001Z -o 'D:\\MigrationBackupTEST' -m true
#  Created by Kapil Kabra on 4/23/21.
#  $ mongodump --host 10.100.30.132 --port 27017 --username wruat3 --password wruat3 --db wruat3  --collection assignmentHistory --query '{ "$and": [{"sourceCompany.companyCode":{ "$in": ["KLRUAT","STC","TC123"] }},{"createdTime.dttm":{ "$gt": { "$date": "2020-07-01T00:00:00.001Z" }}}]}' --out=assthistory


#### reading variable 
usage()
{
	echo -e "\n---------- Help ----------------- \n"	
	echo -e "Usage: $0 -e db_host -u dbuser -p dbpassword -d dbschema -c company -r RUNDATE(yyyy-mm-DDThh:MM:SS.001Z) -o dump folder path  \n\n"
        echo -e ' ./ScriptBackup.sh -e 10.100.30.128 -u wruat3 -p wruat3 -d wruat3 -c ALEXTAXI,KLRUAT -r 2019-03-01T00:00:00.001Z -o output'
}
SCRIPT_ARGS=$#
if [ "${SCRIPT_ARGS}" -lt "5" ]
then
	echo -e "\n only ${SCRIPT_ARGS} arguments?\n"
	usage
	exit 1
fi

COMPANYNAME_LIST=""

while getopts p:e:u:d:c:r:o:a:m:t: option
do
        case "${option}"
        in
       	    p) DBPWD=${OPTARG};;
			e) DBHOST=${OPTARG};;
            u) DBUSER=${OPTARG};;
            d) DBSCHEMA=${OPTARG};;
            c) COMPANYNAME=${OPTARG};;
            r) RUNDATE=${OPTARG};;
			a) ASSTHSTDATE=${OPTARG};;
            o) OUTPUT_FOLDER=${OPTARG};;
			m) ISMASTER=${OPTARG};;
			t) TMPL_DATE=${OPTARG};;
        esac
done

echo "COMPANYNAME:: ${COMPANYNAME}"
echo $DBHOST $DBUSER $DBPWD $DBSCHEMA $RUNDATE $ASSTHSTDATE

#COMPANYNAME_LIST Iteration
arr=(${COMPANYNAME//,/ })
for i in "${!arr[@]}"
do
   echo "ParentCompanyCode index $i ${arr[$i]}"
   if [[ $i = 0 ]]
   then
		echo "first index"
		COMPANYNAME_LIST+="\"${arr[$i]}\""
   #elif [[ $i = $((${#arr[@]}-1)) ]]
   #then
   #	echo "Last index"
	#	COMPANYNAME_LIST+="'${arr[$i]}'"
   else
		COMPANYNAME_LIST+=",\"${arr[$i]}\"" 
   fi
   # or do whatever with individual element of the array
done

echo "COMPANYNAMEs:: ${COMPANYNAME_LIST}"

#### reading variable 

if [ "$ISMASTER" == "true" ]
then
	mongodump --host $DBHOST --port 27017 --username $DBUSER --password $DBPWD --db $DBSCHEMA  --collection settingDefinition  --out=${OUTPUT_FOLDER}
	mongodump --host $DBHOST --port 27017 --username $DBUSER --password $DBPWD --db $DBSCHEMA  --collection systemSettings --query  '{"entity":"SYSTEM_CODE"}' --out=${OUTPUT_FOLDER}
	echo "settingDefinition and systemSettings backup complete"
	exit 0
fi

CONTRACT_QRY="mongodump --host $DBHOST --port 27017 --username $DBUSER --password $DBPWD --db $DBSCHEMA  --collection contract --query '{\"company.companyCode\":{ \"\$in\": [$COMPANYNAME_LIST] }}' --out=${OUTPUT_FOLDER}"
echo -e  "${CONTRACT_QRY}"
eval "${CONTRACT_QRY}"
	PROCESS_STATUS=$?
	if [ "${PROCESS_STATUS}" -eq "0" ]
	then
		echo "----CONTRACT_QRY backup complete----"
	fi

RIDER_QRY="mongodump --host $DBHOST --port 27017 --username $DBUSER --password $DBPWD --db $DBSCHEMA  --collection rider --query '{\"company.companyCode\":{ \"\$in\": [$COMPANYNAME_LIST] }}' --out=${OUTPUT_FOLDER}"
echo -e  "${RIDER_QRY}"
eval "${RIDER_QRY}"
	PROCESS_STATUS=$?
	if [ "${PROCESS_STATUS}" -eq "0" ]
	then
		echo "----RIDER_QRY backup complete----"
	fi

#ROUTE_QRY="mongodump --host $DBHOST --port 27017 --username $DBUSER --password $DBPWD --db $DBSCHEMA  --collection route --query '{ \"\$and\": [{\"company.companyCode\":{ \"\$in\": [$COMPANYNAME_LIST] }},{\"schStartTime.dttm\":{ \"\$gte\": { \"\$date\": \"$RUNDATE\" }}}]}' --out=${OUTPUT_FOLDER}"
#echo -e  "${ROUTE_QRY}"
#eval "${ROUTE_QRY}"
#	PROCESS_STATUS=$?
#	if [ "${PROCESS_STATUS}" -eq "0" ]
#	then
#		echo "----ROUTE_QRY backup complete----"
#	fi

ROUTEDEEF_QRY="mongodump --host $DBHOST --port 27017 --username $DBUSER --password $DBPWD --db $DBSCHEMA  --collection routeDefinition --query '{\"company.companyCode\":{ \"\$in\": [$COMPANYNAME_LIST] }}' --out=${OUTPUT_FOLDER}"
echo -e  "${ROUTEDEEF_QRY}"
eval "${ROUTEDEEF_QRY}"
	PROCESS_STATUS=$?
	if [ "${PROCESS_STATUS}" -eq "0" ]
	then
		echo "----ROUTEDEEF_QRY backup complete----"
	fi
	
SERVICEZONE_QRY="mongodump --host $DBHOST --port 27017 --username $DBUSER --password $DBPWD --db $DBSCHEMA  --collection serviceZone --query '{\"company.companyCode\":{ \"\$in\": [$COMPANYNAME_LIST] }}' --out=${OUTPUT_FOLDER}"
echo -e  "${SERVICEZONE_QRY}"
eval "${SERVICEZONE_QRY}"
	PROCESS_STATUS=$?
	if [ "${PROCESS_STATUS}" -eq "0" ]
	then
		echo "----SERVICEZONE_QRY backup complete----"
	fi

TRIPSUMMARY_QRY="mongodump --host $DBHOST --port 27017 --username $DBUSER --password $DBPWD --db $DBSCHEMA  --collection tripSummary --query '{\"company.companyCode\":{ \"\$in\": [$COMPANYNAME_LIST] }}' --out=${OUTPUT_FOLDER}"
echo -e  "${TRIPSUMMARY_QRY}"
eval "${TRIPSUMMARY_QRY}"
	PROCESS_STATUS=$?
	if [ "${PROCESS_STATUS}" -eq "0" ]
	then
		echo "----TRIPSUMMARY backup complete----"
	fi
	
VIZZONHASH_QRY="mongodump --host $DBHOST --port 27017 --username $DBUSER --password $DBPWD --db $DBSCHEMA  --collection vizzonHash --query '{\"company.companyCode\":{ \"\$in\": [$COMPANYNAME_LIST] }}' --out=${OUTPUT_FOLDER}"
echo -e  "${VIZZONHASH_QRY}"
eval "${VIZZONHASH_QRY}"
	PROCESS_STATUS=$?
	if [ "${PROCESS_STATUS}" -eq "0" ]
	then
		echo "----VIZZONHASH_QRY backup complete----"
	fi
	

LOS_QRY="mongodump --host $DBHOST --port 27017 --username $DBUSER --password $DBPWD --db $DBSCHEMA  --collection levelOfService --query '{\"company.companyCode\":{ \"\$in\": [$COMPANYNAME_LIST] }}' --out=${OUTPUT_FOLDER}"
echo -e  "${LOS_QRY}"
eval "${LOS_QRY}"
	PROCESS_STATUS=$?
	if [ "${PROCESS_STATUS}" -eq "0" ]
	then
		echo "----LOS_QRY backup complete----"
	fi
	
PRESCHEDULE_QRY="mongodump --host $DBHOST --port 27017 --username $DBUSER --password $DBPWD --db $DBSCHEMA  --collection preschedule --query '{\"company.companyCode\":{ \"\$in\": [$COMPANYNAME_LIST] }}' --out=${OUTPUT_FOLDER}"
echo -e  "${PRESCHEDULE_QRY}"
eval "${PRESCHEDULE_QRY}"
	PROCESS_STATUS=$?
	if [ "${PROCESS_STATUS}" -eq "0" ]
	then
		echo "----PRESCHEDULE_QRY backup complete----"
	fi

PROFILE_QRY="mongodump --host $DBHOST --port 27017 --username $DBUSER --password $DBPWD --db $DBSCHEMA  --collection profile --query '{\"company.companyCode\":{ \"\$in\": [$COMPANYNAME_LIST] }}' --out=${OUTPUT_FOLDER}"
echo -e  "${PROFILE_QRY}"
eval "${PROFILE_QRY}"
	PROCESS_STATUS=$?
	if [ "${PROCESS_STATUS}" -eq "0" ]
	then
		echo "----PROFILE_QRY backup complete----"
	fi

SYSSETTINGS_QRY="mongodump --host $DBHOST --port 27017 --username $DBUSER --password $DBPWD --db $DBSCHEMA  --collection systemSettings --query '{\"companyCode\":{ \"\$in\": [$COMPANYNAME_LIST] }}' --out=${OUTPUT_FOLDER}"
echo -e  "${SYSSETTINGS_QRY}"
eval "${SYSSETTINGS_QRY}"
	PROCESS_STATUS=$?
	if [ "${PROCESS_STATUS}" -eq "0" ]
	then
		echo "----SYSSETTINGS_QRY backup complete----"
	fi
	
#ASSTHISTORY_QRY="mongodump --host $DBHOST --port 27017 --username $DBUSER --password $DBPWD --db $DBSCHEMA  --collection assignmentHistory --query '{\"createdTime.dttm\":{ \"\$gte\": { \"\$date\": \"$ASSTHSTDATE\" }}}' --out=${OUTPUT_FOLDER}"
#echo -e  "${ASSTHISTORY_QRY}"
#eval "${ASSTHISTORY_QRY}"
#	PROCESS_STATUS=$?
#	if [ "${PROCESS_STATUS}" -eq "0" ]
#	then
#		echo "----ASSTHISTORY_QRY backup complete----"
#	fi

VIZZON_TEMPLATE_QRY="mongodump --host $DBHOST --port 27017 --username $DBUSER --password $DBPWD --db $DBSCHEMA  --collection vizzon --query '{ \"\$and\": [{\"entity\":\"TRIP\",\"vizzonInfo.company.companyCode\":{ \"\$in\": [$COMPANYNAME_LIST] },\"vizzonInfo.schedule.preScheduleId\":{\"\$exists\":1},\"vizzonInfo.schedule.templateId\":{\"\$exists\":0},\"vizzonInfo.tripInfo.appointmentTime.dttm\":{ \"\$gt\": { \"\$date\": \"$TMPL_DATE\" }}}]}' --out=${OUTPUT_FOLDER}"
echo -e  "${VIZZON_TEMPLATE_QRY}"
eval "${VIZZON_TEMPLATE_QRY}"
	PROCESS_STATUS=$?
	if [ "${PROCESS_STATUS}" -eq "0" ]
	then
		echo "----VIZZON_TEMPLATE_QRY backup complete----"
	fi


