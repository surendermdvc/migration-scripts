#!/bin/sh

#  Script.sh
#  
#
#  Created by vkvishwa on 07/14/21.
#  
# $ sh VizzonScriptBackupTest.sh -h 10.100.30.132 -u wruat3 -p wruat3 -d wruat3 -c SPKTEST12,WTC01,STC -t SPKTEST13 -r 2021-07-05T00:00:00.001Z -o 'D:\\MigrationBackupTEST'
#### reading variable 
usage()
{
	echo -e "\n---------- Help ----------------- \n"	
	echo -e "Usage: $0 -e db_host -u dbuser -p dbpassword -d dbschema -c company -r RUNDATE(yyyy-mm-DDThh:MM:SS.001Z) -o dump folder path  \n\n"
        echo -e ' ./ScriptBackup.sh -e 10.100.30.128 -u wruat3 -p wruat3 -d wruat3 -c ALEXTAXI,KLRUAT -r 2019-03-01T00:00:00.001Z -o output'
}
SCRIPT_ARGS=$#
if [ "${SCRIPT_ARGS}" -lt "5" ]
then
	echo -e "\n only ${SCRIPT_ARGS} arguments?\n"
	usage
	exit 1
fi

PARENTCOMPANY_LIST=""
TPCOMPANY_LIST=""

while getopts p:h:u:d:c:t:r:o: option
do
        case "${option}"
        in
       	    p) DBPWD=${OPTARG};;
			h) DBHOST=${OPTARG};;
            u) DBUSER=${OPTARG};;
            d) DBSCHEMA=${OPTARG};;
            c) PARENTCOMPANY=${OPTARG};;
			t) TPCOMPANY=${OPTARG};;
            r) RUNDATE=${OPTARG};;
            o) OUTPUT_FOLDER=${OPTARG};;
        esac
done
echo $DBHOST $DBUSER $DBPWD $DBSCHEMA $PARENTCOMPANY $TPCOMPANY $RUNDATE


#PARENTCOMPANY Iteration
arr=(${PARENTCOMPANY//,/ })
for i in "${!arr[@]}"
do
   echo "ParentCompanyCode index $i ${arr[$i]}"
   if [[ $i = 0 ]]
   then
		echo "first index"
		PARENTCOMPANY_LIST+="\"${arr[$i]}\""
   #elif [[ $i = $((${#arr[@]}-1)) ]]
   #then
	#	echo "Last index"
	#	PARENTCOMPANY_LIST+=",'${arr[$i]}'"
   else
		PARENTCOMPANY_LIST+=",\"${arr[$i]}\"" 
   fi
   # or do whatever with individual element of the array
done

echo "----------PARENTCOMPANYs:: ${PARENTCOMPANY_LIST}"

#TPCOMPANY Iteration
arr=(${TPCOMPANY//,/ })
for i in "${!arr[@]}"
do
   echo "TPCompanyCode index $i ${arr[$i]}"
   if [[ $i = 0 ]]
   then
		echo "first index"
		TPCOMPANY_LIST+="\"${arr[$i]}\""
   #elif [[ $i = $((${#arr[@]}-1)) ]]
   #then
	#	echo "Last index"
	#	TPCOMPANY_LIST+=",'${arr[$i]}'"
   else
		TPCOMPANY_LIST+=",\"${arr[$i]}\"" 
   fi
   # or do whatever with individual element of the array
done

echo "----------TPCOMPANYs:: ${TPCOMPANY_LIST}"

#### reading variable 

if [[ ! -z "${PARENTCOMPANY_LIST}" &&  -z "${TPCOMPANY_LIST}" ]]
then
 
	VIZZON_QRY="mongodump --host $DBHOST --port 27017 --username $DBUSER --password $DBPWD --db $DBSCHEMA  --collection vizzon --query '{ \"\$and\": [{\"entity\":\"TRIP\",\"vizzonInfo.company.companyCode\":{ \"\$in\": [$PARENTCOMPANY_LIST] }},{\"vizzonInfo.tripInfo.appointmentTime.dttm\":{ \"\$gt\": { \"\$date\": \"$RUNDATE\" }}}]}' --out=${OUTPUT_FOLDER}"
	echo "Executing without tp company"
	echo -e  "${VIZZON_QRY}"
	eval "${VIZZON_QRY}"
		PROCESS_STATUS=$?
		if [ "${PROCESS_STATUS}" -eq "0" ]
		then
			echo "VIZZON_QRY backup complete"
		fi
else
	VIZZON_QRY="mongodump --host $DBHOST --port 27017 --username $DBUSER --password $DBPWD --db $DBSCHEMA  --collection vizzon --query '{ \"\$and\": [{\"entity\":\"TRIP\",\"vizzonInfo.company.companyCode\":{ \"\$in\": [$PARENTCOMPANY_LIST] }}, {\"vizzonInfo.tp.companyCode\":{ \"\$in\": [$TPCOMPANY_LIST] }}, {\"vizzonInfo.tripInfo.appointmentTime.dttm\":{ \"\$gt\": { \"\$date\": \"$RUNDATE\" }}}]}' --out=${OUTPUT_FOLDER}"
	echo "Executing with parent & tp company"
	echo -e  "${VIZZON_QRY}"
	eval "${VIZZON_QRY}"
		PROCESS_STATUS=$?
		if [ "${PROCESS_STATUS}" -eq "0" ]
		then
			echo "VIZZON_QRY backup complete"
		fi
fi
