usage()
{
    echo -e "\n---------- help ----------------- \n"    
    echo -e "Usage: $0 [OPTIONS] -h <host> -u <username> -p <passwor> -d <dbname> -c <companyCodes> -f <filename> \n\n"
    echo -e "Example: $0 -h 127.0.0.1 -u root -p root -d wrdev -c CIRC"
}

SCRIPT_ARGS=$#
if [ "${SCRIPT_ARGS}" -lt "3" ]
then
    echo -e "\n only ${SCRIPT_ARGS}? \n"
    usage
    exit
fi

HOST=NA
DBNAME=NA
USERNAME=NA
PASSWORD=NA
COMPANY_CODES=NA
FILE_NAME=NA
COMPANY_LIST=""
PORT=3306

while getopts h:u:p:d:f:c:t: option
do
        case "${option}"
        in
                h) HOST=${OPTARG};;
                u) USERNAME=${OPTARG};;
                p) PASSWORD=${OPTARG};;
                d) DBNAME=${OPTARG};;
                c) COMPANY_CODES=${OPTARG};;
                f) FILE_NAME=${OPTARG};;
                t) PORT=${OPTARG};;
        esac
done

echo "DATABASE:: ${DBNAME}"
echo "COMPANY_CODES:: ${COMPANY_CODES}"


arr=(${COMPANY_CODES//,/ })

for i in "${!arr[@]}"
do
   echo "CompanyCode index $i ${arr[$i]}"
   if [[ $i = 0 ]]
   then
    echo "first index"
    COMPANY_LIST+="'${arr[$i]}'"
   #elif [[ $i = $((${#arr[@]}-1)) ]]
   #then
   # echo "Last index"
   # COMPANY_LIST+="'${arr[$i]}'"
   else
    COMPANY_LIST+=",'${arr[$i]}'" 
   fi
   # or do whatever with individual element of the array
done

echo "CompanyList :: $COMPANY_LIST"

# Master data extract
mysqldump -h${HOST} -u${USERNAME} -p${PASSWORD} -P${PORT} ${DBNAME} --no-create-info --lock-tables=false --tables AlertConfig AppView Application AutoCounter BusinessPartner Carrier CodeDetail ColumnSecurity Company CompanyDeviceType CredentialDetail CredentialSetting CustomAttributeDef Customer DashboardAccess DashboardPortletDetail DeviceLocationConfig DocumentRef EmailConfig EmergencyContact EventTemplate EventEndPointConfig Facility FldSecurityConfig IntEndPointConfig IntModelMap IntServiceAccess MDMApplication MDMCertificate MsgTemplate OAuthServiceProvider PhoneNumberConfig RiderCommunicationSchedule RouteCreationTemplate SecurityPolicy SettingTemplate SMSConfig STBL_Contract  STBL_ServiceZone STBL_Surcharge  SyncMaster TermsOfUse TOUCompanyAccess UserCompany UserGroup Vehicle VehicleType TimeSlot TripHash TripSource TripSourceAccount TripSourceInterest WorkType --where="CompanyId  in (select CompanyId from Company where CompanyCode in (${COMPANY_LIST}))" > ${FILE_NAME}_company_master_data.sql

# Document data extract
mysqldump -h${HOST} -u${USERNAME} -p${PASSWORD} -P${PORT} ${DBNAME} --no-create-info --lock-tables=false --tables Document --where="DocumentType != '01' and CompanyId  in (select CompanyId from Company where CompanyCode in (${COMPANY_LIST}))" > ${FILE_NAME}_company_document_data.sql

mysqldump -h${HOST} -u${USERNAME} -p${PASSWORD} -P${PORT} ${DBNAME} --no-create-info --lock-tables=false --tables ApplicationVersion AppUserGroup --where="AppID in (select AppID from Application where CompanyId in (select CompanyId from Company where CompanyCode in (${COMPANY_LIST})))" > ${FILE_NAME}_app_data.sql

# Host Company filters and preferences
mysqldump -h${HOST} -u${USERNAME} -p${PASSWORD} -P${PORT} ${DBNAME} --no-create-info --lock-tables=false --tables CustomListDef ListFormatterDef Preference --where="(CustomType = '01' or CustomType is null) and CustomId in (select CompanyId from Company where CompanyCode in (${COMPANY_LIST}))" > ${FILE_NAME}_preference_filters_data.sql

mysqldump -h${HOST} -u${USERNAME} -p${PASSWORD} -P${PORT} ${DBNAME} --no-create-info --lock-tables=false --tables ListViewDef --where="CustomListDefId in (select CustomListDefId from CustomListDef where CustomType='01' and CustomId in (select CompanyId from Company where CompanyCode in (${COMPANY_LIST})))" > ${FILE_NAME}_company_listViewDef_data.sql


# Host Company Event configuration
mysqldump -h${HOST} -u${USERNAME} -p${PASSWORD} -P${PORT} ${DBNAME} --no-create-info --lock-tables=false --tables EventTemplateAction --where="EventTemplateId in (select EventTemplateId from EventTemplate where CompanyId in (select CompanyId from Company where CompanyCode in (${COMPANY_LIST})))" > ${FILE_NAME}_event_data.sql

# Host Company User data extraction
mysqldump -h${HOST} -u${USERNAME} -p${PASSWORD} -P${PORT} ${DBNAME} --no-create-info --lock-tables=false --tables AlertRecipient User UserGroupDetail UserRole UserSchedule UserSecurityAnswer --where="UserId in (select UserId from User where DefaultCompanyId in (select CompanyId from Company where CompanyCode in (${COMPANY_LIST})))" > ${FILE_NAME}_user_data.sql

# User filter view preference data extraction
mysqldump -h${HOST} -u${USERNAME} -p${PASSWORD} -P${PORT} ${DBNAME} --no-create-info --lock-tables=false --tables CustomListDef ListFormatterDef Preference ListViewDefPref --where="CustomType='03' and CustomId in (select UserId from User where DefaultCompanyId in (select CompanyId from Company where CompanyCode in (${COMPANY_LIST})))" > ${FILE_NAME}_user_filter_preference_data.sql

mysqldump -h${HOST} -u${USERNAME} -p${PASSWORD} -P${PORT} ${DBNAME} --no-create-info --lock-tables=false --tables ListViewDef --where="CustomListDefId in (select CustomListDefId from CustomListDef where CustomType='03' and CustomId in (select UserId from User where DefaultCompanyId in (select CompanyId from Company where CompanyCode in (${COMPANY_LIST}))))" > ${FILE_NAME}_user_listViewDef_data.sql


# Company Endpoint certs extraction
mysqldump -h${HOST} -u${USERNAME} -p${PASSWORD} -P${PORT} ${DBNAME} --no-create-info --lock-tables=false --tables IntEndpointCertificate --where="IntEndPointConfigId in (select IntEndPointConfigId from IntEndPointConfig where CompanyId  in (select CompanyId from Company where CompanyCode in (${COMPANY_LIST})))" > ${FILE_NAME}_ep_data.sql

mysqldump -h${HOST} -u${USERNAME} -p${PASSWORD} -P${PORT} ${DBNAME} --no-create-info --lock-tables=false --tables DashboardConfig --where="OwnerId in (select UserId from User where DefaultCompanyId in (select CompanyId from Company where CompanyCode in (${COMPANY_LIST})))" > ${FILE_NAME}_dashboard_data.sql

mysqldump -h${HOST} -u${USERNAME} -p${PASSWORD} -P${PORT} ${DBNAME} --no-create-info --lock-tables=false --tables VehicleLos --where="VehicleTypeId in (select VehicleTypeId from VehicleType where CompanyId in (select CompanyId from Company where CompanyCode in (${COMPANY_LIST})))" > ${FILE_NAME}_vehicle_data.sql

cat ${FILE_NAME}*.sql > ${FILE_NAME}.sql

gzip ${FILE_NAME}.sql
