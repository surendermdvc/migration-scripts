# sh jobrtr_migration.sh -h 10.100.30.133 -u backup -p backuppwd  -d wruat_jobrtr -f batchjob -c 1 -m true -n '2021-07-15 16:00:26'

usage()
{
    echo -e "\n---------- help ----------------- \n"    
    echo -e "Usage: $0 [OPTIONS] -h <host> -u <username> -p <passwor> -d <dbname> -f <filename> -c <companycodes> \n\n"
    echo -e "Example: $0 -h 127.0.0.1 -u root -p root -d wrdev -f master_backup -c WTC01,TC123 -m true -n 2021-07-15 16:00:26"
}

SCRIPT_ARGS=$#
if [ "${SCRIPT_ARGS}" -lt "3" ]
then
    echo -e "\n only ${SCRIPT_ARGS}? \n"
    usage
    exit
fi

HOST=NA
DBNAME=NA
USERNAME=NA
PASSWORD=NA
COMPANY_IDS=NA
FILE_NAME=NA
ISMASTER=NA
EXECDATE=NA
PORT=3306

while getopts h:u:p:d:f:c:m:n:t: option
do
        case "${option}"
        in
                h) HOST=${OPTARG};;
                u) USERNAME=${OPTARG};;
                p) PASSWORD=${OPTARG};;
                d) DBNAME=${OPTARG};;
                f) FILE_NAME=${OPTARG};;
				c) COMPANY_IDS=${OPTARG};;
				m) ISMASTER=${OPTARG};;
				n) EXECDATE=${OPTARG};;
				t) PORT=${OPTARG};;
        esac
done

echo "DATABASE:: ${DBNAME}"


# Master data extract
if [ "$ISMASTER" == "true" ]
then
	echo "Extracting JObrtr database tables defination."
	mysqldump -h${HOST} -u${USERNAME} -p${PASSWORD} -P${PORT} --lock-tables=false -d ${DBNAME} > ${FILE_NAME}_jobrtr_create_tables.sql
	echo "Extracting jobrtr master data tables information."
	mysqldump -h${HOST} -u${USERNAME} -p${PASSWORD} -P${PORT} ${DBNAME} --lock-tables=false --no-create-info --skip-add-locks=true --tables Job > ${FILE_NAME}_jobrtr_master_data.sql
		
else
	#Company specific extract
	mysqldump -h${HOST} -u${USERNAME} -p${PASSWORD} -P${PORT} ${DBNAME} --no-create-info --skip-add-locks=true --lock-tables=false --tables JobSchedule --where="CompanyId  in (${COMPANY_IDS})" > ${FILE_NAME}_jobrtr_company_master_data.sql
fi


cat ${FILE_NAME}*.sql > ${FILE_NAME}.sql

gzip ${FILE_NAME}.sql
