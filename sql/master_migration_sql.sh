# sh master_migration_sql.sh -h localhost -u root -p root -d wr21 -f wr21master
# sh parent_company_migration_sql.sh -h localhost -u root -p root -d wr21 -c PPP
# sh data_restore.sh -h localhost -u root -p root -d wr21_backup -f wr21master.sql
# sh data_restore.sh -h localhost -u root -p root -d wr21_backup -f wr21.sql

usage()
{
    echo -e "\n---------- help ----------------- \n"    
    echo -e "Usage: $0 [OPTIONS] -h <host> -u <username> -p <passwor> -d <dbname> -f <filename> \n\n"
    echo -e "Example: $0 -h 127.0.0.1 -u root -p root -d wrdev -f master_backup"
}

SCRIPT_ARGS=$#
if [ "${SCRIPT_ARGS}" -lt "3" ]
then
    echo -e "\n only ${SCRIPT_ARGS}? \n"
    usage
    exit
fi

HOST=NA
DBNAME=NA
USERNAME=NA
PASSWORD=NA
FILE_NAME=NA
PORT=3306

while getopts h:u:p:d:t:f: option
do
        case "${option}"
        in
                h) HOST=${OPTARG};;
                u) USERNAME=${OPTARG};;
                p) PASSWORD=${OPTARG};;
                d) DBNAME=${OPTARG};;
                f) FILE_NAME=${OPTARG};;
                t) PORT=${OPTARG};;
        esac
done

echo "DATABASE:: ${DBNAME}"

echo "Extracting database tables defination."
mysqldump -h${HOST} -u${USERNAME} -p${PASSWORD} -P${PORT} --lock-tables=false -d ${DBNAME} > ${FILE_NAME}_create_tables.sql

 
# Master data extract
echo "Extracting master data tables information."
mysqldump -h${HOST} -u${USERNAME} -p${PASSWORD} -P${PORT} ${DBNAME} --lock-tables=false --skip-add-locks=true --no-create-info --tables ActivityCode AlertMaster AppDeviceEligibility AppExpiry AppOSVersionEligibility CustomMenuTemplate CodeHeader DashboardPortlet DashboardPortletMaster DeviceType EntityColumnDef EntityJoinDef EventAction EventMaster EventMasterAction IntModelMaster IntServiceDtl IntServiceMaster ListColumnDef ListDef ListEntityDef ListJoinDef MDMCommand MDMCommandArg MenuCategoryMaster MenuItemMaster MenuDefinition MicroServiceUrl ModuleMaster MsgMaster  OIntServiceDtl OIntServiceMaster OperatingSystem OSDateTimeFormat OSVersion PDSTransactionResult Privilege Role SecurityQuestionMaster SettingCategory SettingMaster SMSProvider ViewDef > ${FILE_NAME}_master_data.sql

# Master data extract for default filters.
echo "Extracting default filters information."
mysqldump -h${HOST} -u${USERNAME} -p${PASSWORD} -P${PORT} ${DBNAME} --no-create-info --lock-tables=false --skip-add-locks=true --tables ListViewDef --where="CustomListDefId is null" > ${FILE_NAME}_default_filters_data.sql


cat ${FILE_NAME}*.sql > ${FILE_NAME}.sql

gzip ${FILE_NAME}.sql
