usage()
{
    echo -e "\n---------- help ----------------- \n"    
    echo -e "Usage: $0 [OPTIONS] -h <host> -u <username> -p <passwor> -d <dbname> -f <filename> \n\n"
    echo -e "Example: $0 -h 127.0.0.1 -u root -p root -d wrdev -f migration.sql.gz"
}

SCRIPT_ARGS=$#
if [ "${SCRIPT_ARGS}" -lt "3" ]
then
    echo -e "\n only ${SCRIPT_ARGS}? \n"
    usage
    exit
fi

HOST=NA
DBNAME=NA
USERNAME=NA
PASSWORD=NA
FILE_NAME=NA
PORT=3306

while getopts h:u:p:d:f:l:e:c:t: option
do
        case "${option}"
        in
                h) HOST=${OPTARG};;
                u) USERNAME=${OPTARG};;
                p) PASSWORD=${OPTARG};;
                d) DBNAME=${OPTARG};;
                f) FILE_NAME=${OPTARG};;
                t) PORT=${OPTARG};;
        esac
done

echo "DATABASE :: ${DBNAME}"
echo "FILE_NAME :: ${FILE_NAME}"

gunzip ${FILE_NAME}.gz

mysql --ssl-mode=disabled -h${HOST} -u${USERNAME} -p${PASSWORD} ${DBNAME} < ${FILE_NAME}