# sh nfs_migration.sh -f '/mnt/devnfs/docker_vol/wruat' -c 'NUVZWRUAT' -m true -n foldername

usage()
{
    echo -e "\n---------- help ----------------- \n"    
    echo -e "Example: sh nfs_migration.sh -f '<folder path>' -o <output_folder> -c 'NUVZWRUAT' -m true"
}

SCRIPT_ARGS=$#
if [ "${SCRIPT_ARGS}" -lt "3" ]
then
    echo -e "\n only ${SCRIPT_ARGS}? \n"
    usage
    exit
fi

while getopts f:c:m:n: option
do
        case "${option}"
        in
                f) FOLDERPATH=${OPTARG};;
				c) COMPANY_CODES=${OPTARG};;
				m) ISMASTER=${OPTARG};;
				n) FOLDERNAME=${OPTARG}
        esac
done

echo "NFS FOLDER PATH:: ${FOLDERPATH}"

#sudo rm -rf ~/nfsbackup/${FOLDERNAME}

############## Copying master Data ################
#create master data folder
# ${FOLDERPATH} - /mnt/devnfs/docker_vol/wruat

if [ "$ISMASTER" == "true" ]
then
	mkdir -p ~/nfsbackup/${FOLDERNAME}/menutemplatelogo
	mkdir -p ~/nfsbackup/${FOLDERNAME}/menu
	mkdir -p ~/nfsbackup/${FOLDERNAME}/velocity-templates
	mkdir -p ~/nfsbackup/${FOLDERNAME}/xsl
	mkdir -p ~/nfsbackup/${FOLDERNAME}/samplemappingtemplate
	mkdir -p ~/nfsbackup/${FOLDERNAME}/apps

	# go o folder path
	cd ${FOLDERPATH}
	echo "PWD : " 
	pwd
	#copy master data content to master-backup path*
	sudo cp -r di_dist/data/menutemplatelogo/* ~/nfsbackup/${FOLDERNAME}/menutemplatelogo
	sudo cp -r di_dist/data/menu/* ~/nfsbackup/${FOLDERNAME}/menu
	sudo cp -r di_dist/data/velocity-templates/* ~/nfsbackup/${FOLDERNAME}/velocity-templates
	sudo cp -r di_dist/data/xsl/* ~/nfsbackup/${FOLDERNAME}/xsl
	sudo cp -r di_dist/data/samplemappingtemplate/* ~/nfsbackup/${FOLDERNAME}/samplemappingtemplate
	sudo cp -r di_dist/data/apps/* ~/nfsbackup/${FOLDERNAME}/apps
fi

############ Copying company specific data ##############

# /mnt/devnfs/docker_vol/wruat/di_dist/data/uploads/doc

if [ ! -z "${COMPANY_CODES}" ]
then
	cd ~
	
	mkdir -p ~/nfsbackup/${FOLDERNAME}/logo
#	mkdir -p ~/nfsbackup/${FOLDERNAME}/uploads/doc
	mkdir -p ~/nfsbackup/${FOLDERNAME}/uploads/Masterdoc

	arr=(${COMPANY_CODES//,/ })
	for i in "${!arr[@]}"
	do
	   echo "CompanyCode index $i ${arr[$i]}"
	   
	   #Copying the logo folder for the company
	   if [ -d "${FOLDERPATH}/di_dist/data/logo/${arr[$i]}" ]
	   then
			sudo cp -r ${FOLDERPATH}/di_dist/data/logo/${arr[$i]} ~/nfsbackup/${FOLDERNAME}/logo/
	   fi
	   	   
	   #copying uploads/doc folder for the company
	   #if [ -d "${FOLDERPATH}/di_dist/data/uploads/doc/${arr[$i]}" ]
	   #then
	   #	sudo cp -r ${FOLDERPATH}/di_dist/data/uploads/doc/${arr[$i]} ~/nfsbackup/${FOLDERNAME}/uploads/doc/${arr[$i]}
	   #fi
	   
	   #copying uploads/Masterdoc folder for the company
	   if [ -d "${FOLDERPATH}/di_dist/data/uploads/Masterdoc/${arr[$i]}" ]
	   then
			sudo cp -r ${FOLDERPATH}/di_dist/data/uploads/Masterdoc/${arr[$i]} ~/nfsbackup/${FOLDERNAME}/uploads/Masterdoc/${arr[$i]}
	   fi
	   
	done
fi


# nfsbackup/nfs1/***
# nfsbackup/nfs2/***
# nfsbackup/nfs3/***
# nfsbackup/nfs1.tar.gz

cd ~/nfsbackup
ls ${FOLDERNAME}/
sudo tar -cvzf ${FOLDERNAME}.tar.gz -C ${FOLDERNAME}/ .
sudo rm -rf ${FOLDERNAME}

#git init
#git remote add origin https://bitbucket.org/wellryde21/wr_nfs_backup.git
#git pull origin master
#sudo git add -A
#sudo git commit -m 'NFS Backup commit'
#sudo git push -u origin master 



