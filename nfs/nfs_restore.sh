# sh nfs_migration.sh -f '<outputfolder path>' -n folder_name

usage()
{
    echo -e "\n---------- help ----------------- \n"    
    echo -e "Example: sh nfs_migration.sh -f '<outputfolder path>'"
}

SCRIPT_ARGS=$#
if [ "${SCRIPT_ARGS}" -lt "3" ]
then
    echo -e "\n only ${SCRIPT_ARGS}? \n"
    usage
    exit
fi

FOLDERPATH=NA
COMPANY_CODES=NA

while getopts f:n: option
do
        case "${option}"
        in
			f) FOLDERPATH=${OPTARG};;
			n) FOLDERNAME=${OPTARG};;
				
        esac
done

echo "NFS FOLDER PATH:: ${FOLDERPATH}"

#create master data folder
# ${FOLDERPATH} - /mnt/devnfs/docker_vol/wruat/di_dist/data

cd ~

#sudo rm -rf wr_nfs_backup
#git clone https://bitbucket.org/wellryde21/wr_nfs_backup.git

sudo mkdir -p ${FOLDERPATH}/menutemplatelogo
sudo mkdir -p ${FOLDERPATH}/velocity-templates
sudo mkdir -p ${FOLDERPATH}/xsl
sudo mkdir -p ${FOLDERPATH}/samplemappingtemplate
sudo mkdir -p ${FOLDERPATH}/logo
sudo mkdir -p ${FOLDERPATH}/apps
sudo mkdir -p ${FOLDERPATH}/uploads

# go o folder path
#cd ${FOLDERPATH}

#Extract the <FOLDERNAME> compressed file from git.
sudo tar -xvzf wr_nfs_backup/${FOLDERNAME}.tar.gz -C wr_nfs_backup/

#copy master data content to master-backup path*
sudo cp -r wr_nfs_backup/menutemplatelogo/* ${FOLDERPATH}/menutemplatelogo
sudo cp -r wr_nfs_backup/velocity-templates/* ${FOLDERPATH}/velocity-templates
sudo cp -r wr_nfs_backup/xsl/* ${FOLDERPATH}/xsl
sudo cp -r wr_nfs_backup/samplemappingtemplate/* ${FOLDERPATH}/samplemappingtemplate
sudo cp -r wr_nfs_backup/logo/* ${FOLDERPATH}/logo
sudo cp -r wr_nfs_backup/apps/* ${FOLDERPATH}/apps
sudo cp -r wr_nfs_backup/uploads/* ${FOLDERPATH}/uploads
#sudo cp -r wr_nfs_backup/uploads/doc/* ${FOLDERPATH}/uploads/doc
#sudo cp -r wr_nfs_backup/uploads/Masterdoc/* ${FOLDERPATH}/uploads/Masterdoc

